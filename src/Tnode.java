import java.sql.SQLOutput;
import java.util.*;

public class Tnode {
   private static final ArrayList<String> operators = new ArrayList<>(Arrays.asList("+", "-", "*", "/"));
   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   public Tnode (String name, Tnode firstChild, Tnode nextSibling) {
      this.name = name;
      this.firstChild = firstChild;
      this.nextSibling = nextSibling;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Tnode getFirstChild() {
      return firstChild;
   }

   public Tnode getNextSibling() {
      return nextSibling;
   }

   public void setFirstChild(Tnode firstChild) {
      this.firstChild = firstChild;
   }

   public void setNextSibling(Tnode nextSibling) {
      this.nextSibling = nextSibling;
   }

   @Override
   public String toString() {
      StringBuilder b = new StringBuilder();
      if (operators.contains(this.getName())) {
         b.append(this.getName()).append("(");
         b.append(this.getFirstChild().toString()).append(",");
         b.append(this.getFirstChild().getNextSibling().toString());
         b.append(")");return b.toString();
      } else {
         return b.append(this.getName()).toString();
      }
   }

   public static Tnode buildFromRPN (String pol) {
      if (pol == null || pol.equals("") || pol.matches("\\s+")) {
         throw new RuntimeException("Avaldis puudub");
      }

      LinkedList<String> elements = new LinkedList<>(Arrays.asList(pol.split("\\s+")));
      for (String element : elements) {
         switch (element) {
            case "+":
            case "-":
            case "*":
            case "/":
            case "SWAP":
            case "ROT":
               continue;
            default:
               try {
                  Integer.valueOf(element);
               } catch (NumberFormatException e) {
                  throw new RuntimeException("Vigane element '" + element + "' avaldises '" + pol + "'");
               }

         }
      }
      boolean noSWAPandROT;

      do {
         noSWAPandROT = true;
         for (int i = 0; i < elements.size(); i++) {
            if (elements.get(i).equals("SWAP") || elements.get(i).equals("ROT")) {
               noSWAPandROT = false;
               LinkedList<String> toSort = new LinkedList<>();
               for (int j = 0; j < i + 1; j++) {
                  toSort.addLast(elements.get(j));
               }
               LinkedList<String> firstPart = MakeSWAPandROT(toSort, pol);
               LinkedList<String> secondPart = new LinkedList<>();
               for (int j = i + 1; j < elements.size(); j++) {
                  secondPart.addLast(elements.get(j));
               }
               firstPart.addAll(secondPart);
               elements = firstPart;
               break;
            }
         }
      } while (!noSWAPandROT);
      Tnode root = buildNode(elements, pol);
      if (elements.size() > 0) {
         throw new RuntimeException("Liiga palju arve avaldises " + pol);
      }

      return root;
   }

   public static LinkedList<String> MakeSWAPandROT(LinkedList<String> lst, String pol) {
      LinkedList<String> result;
      if (lst.getLast().equals("SWAP")) {
         lst.removeLast();
         try {
            result = SWAP(lst);
         } catch (NoSuchElementException e) {throw new RuntimeException("Ei piisa arve tehte 'SWAP' jaoks avaldises '" + pol + "'");}
      } else {
         lst.removeLast();
         try {
            result = ROT(lst);
         } catch (NoSuchElementException e) {throw new RuntimeException("Ei piisa arve tehte 'ROT' jaoks avaldises '" + pol + "'");}
      }
      return result;
   }

   public static Tnode buildNode(LinkedList<String> cache, String pol) {
      Tnode result = new Tnode(cache.getLast(), null, null);
      cache.removeLast();

      Tnode sibling;

      if (operators.contains(result.name)) {
         if (cache.size() >= 2) {
            sibling = buildNode(cache, pol);

            try {
               result.setFirstChild(buildNode(cache, pol));
               result.getFirstChild().setNextSibling(sibling);
            } catch (NoSuchElementException e) {
               throw new RuntimeException("Ei piisa arve tehte '" + result.getName() + "' jaoks avaldises " + pol + "'");
            }
         } else {
            throw new RuntimeException("Ei piisa arve tehteks " + result.getName() + " avaldises " + pol);
         }
      }

      return result;
   }

   public static LinkedList<String> ROT(LinkedList<String> lst) {
      LinkedList<String> a = new LinkedList<>();
      LinkedList<String> b = new LinkedList<>();
      LinkedList<String> c = new LinkedList<>();
      makeElement(lst, c);
      makeElement(lst, b);
      makeElement(lst, a);

      b.addAll(c);
      b.addAll(a);
      return b;
   }


   public static void makeElement(LinkedList<String> arguments, LinkedList<String> element) {
      int count = 1;
      while (count > 0) {
         if(operators.contains(arguments.getLast())) {
            count += 2;
         }
         element.addFirst(arguments.getLast());
         arguments.removeLast();
         count--;
      }

   }

   public static LinkedList<String> SWAP(LinkedList<String> lst) {
      LinkedList<String> a = new LinkedList<>();
      LinkedList<String> b = new LinkedList<>();
      makeElement(lst, b);
      makeElement(lst, a);
      b.addAll(a);
      return b;
   }

   public static void main (String[] param) {
      System.out.println("ROT");
      //LinkedList<String> arg = new LinkedList<>(Arrays.asList("2", "3", "+", "1", "3"));
      //System.out.println(Tnode.ROT(arg));
      System.out.println("------------------------");
      System.out.println("SWAP");
      //LinkedList<String> arg2 = new LinkedList<>(Arrays.asList("2", "-3", "+", "1", "-", "1", "3", "*"));
      //System.out.println(Tnode.SWAP(arg2));
      System.out.println("------------------------");
      System.out.println("BuildFromRPN");
      String rpn = "2 5 SWAP -";
      System.out.println ("RPN: " + rpn);
      Tnode res = buildFromRPN (rpn);
      System.out.println ("Tree: " + res);

      System.out.println("------------------------");
      System.out.println("BuildFromRPN");
      String rpn1 = "2 5 9 ROT - +";
      System.out.println ("RPN: " + rpn1);
      Tnode res1 = buildFromRPN (rpn1);
      System.out.println ("Tree: " + res1);

      System.out.println("------------------------");
      System.out.println("BuildFromRPN");
      String rpn2 = "2 5 9 ROT + SWAP -";
      System.out.println ("RPN: " + rpn2);
      Tnode res2 = buildFromRPN (rpn2);
      System.out.println ("Tree: " + res2);

      System.out.println("------------------------");
      System.out.println("BuildFromRPN");
      String rpn3 = "2 3 + 7 4 * SWAP 3 1 / ROT + -";
      System.out.println ("RPN: " + rpn3);
      Tnode res3 = buildFromRPN (rpn3);
      System.out.println ("Tree: " + res3);

   }
}

